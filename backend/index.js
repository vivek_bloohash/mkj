const express = require('express');
var cors = require('cors');
var http = require('http');
var axios = require('axios');
var path = require('path');
const Cart = require("./modal/cartschema");

const passport = require('passport');
const router = express.Router({ mergeParams: true });
require('./dbconnection/db');

const app = express();
app.use(express.urlencoded({ limit: '50mb' }));
let server = http.Server(app);
app.use(passport.initialize());
require('./utils/passport')(passport);

app.use(express.json({ limit: '50mb' }));

let makkajproduct = require('./router/routes/productrouter');
let cart = require('./router/routes/cartrouter');
let user = require('./router/routes/user');
let address = require('./router/routes/address');
let orders = require('./router/routes/orderrouter');
let categort = require('./router/routes/categoryrouter');
let banner = require('./router/routes/banner');
let coupon = require('./router/routes/coupanrouter');
let wishlist = require('./router/routes/wishlistrouter');
let adminlogin = require('./router/routes/adminloginrouter');
let review = require('./router/routes/review');
let tax = require('./router/routes/taxrouter');
let aboutus = require('./router/routes/aboutus');
let home = require('./router/routes/home');
let termsandcond = require('./router/routes/termsandcondition');
let privacy = require('./router/routes/privacyrouter');
let returns = require('./router/routes/return');



//let medicinecategory = require('./router/medicines/categoryrouter');
app.use(cors({ credentials: true, origin: "*" }));
app.use(express.json());
app.use('/api', makkajproduct);
app.use('/api', cart);
app.use('/api', user);
app.use('/api', address);
app.use('/api', orders);
app.use('/api', categort);
app.use('/api', banner);
app.use('/api', coupon);
app.use('/api', wishlist);
app.use('/api', adminlogin);
app.use('/api', review);
app.use('/api', tax);
app.use('/api', aboutus);
app.use('/api', home);
app.use('/api', termsandcond);
app.use('/api', privacy);
app.use('/api', returns);


app.use(function (req, res, next) {
	res.setHeader("Access-Control-Allow-Origin", "*");
	res.setHeader("Access-Control-Allow-Headers", "Content-Type");
	res.setHeader("Access-Control-Allow-Credentials", "true");
	res.setHeader("Content-Type", "multipart/form-data");
	next();
});


__dirname = path.resolve();
app.use('/uploads', express.static(path.join(__dirname, '/uploads')));

if (process.env.NODE_ENV === 'production') {
	app.use(express.static(path.join(__dirname, '/frontend/build')));
	app.use(express.static(path.join(__dirname, '/admin/build')));

	app.get('/admin', (req, res) => res.sendFile(path.resolve(__dirname, 'adminend', 'build', 'index.html')));
	app.get('*', (req, res) => res.sendFile(path.resolve(__dirname, 'frontend', 'build', 'index.html')));
} else {
	app.get('/', (req, res) => {
		res.send('API is running....');
	});
}

const generateRandomOrderId = () => {
	let text = "";
	let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	for (let i = 0; i < 100; i++)
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	return text;
}

app.get('/paytabs/:userId/:tax', async (req, res) => {
	Cart.find({ userId: req.params.userId }, (err, user) => {
		if (err) {
			return res.status(500).send({ err: err });
		}
		console.log(user[0]?.products)
		const products = user[0]?.products?.length;
		// console.log(products)
		var tempArray = [];
		for (let i = 0; i < products; i++) {
			// console.log(user[0].products[i].discountprice)
			var price = (user[0]?.products[i]?.discountprice);
			var a = price.map(Number);
			var total = a.reduce(function (a, b) { return a + b; }, 0);
			tempArray.push(total);
		}
		console.log(tempArray);
		let sum = 0;
		for (let i = 0; i < tempArray?.length; i++) {
			sum += tempArray[i];
		}
		sum += parseInt(req.params.tax)
		if (sum < 300) {
			sum += 0;
		}
		console.log(sum);
		// call paytabs api to generate payment link
		const getBreeds = async () => {
			var raw = JSON.stringify({
				"profile_id": 55916,
				"tran_type": "sale",
				"tran_class": "ecom",
				"cart_id": generateRandomOrderId(),
				"cart_currency": "AED",
				"cart_amount": sum,
				"cart_description": "Description of the items/services",
				"paypage_lang": "en",
				"callback": "https://api.makkajperfumes.com/callback",
				"return": "https://api.makkajperfumes.com/callback",
			});
			var headers = {
				'Content-Type': 'application/json',
				'Authorization': 'SDJN9BBMTG-JBBHGLWJDD-WH2J9Z69JW'
			}
			try {
				return await axios.post('https://secure.paytabs.com/payment/request', raw, { headers: headers });
			} catch (error) {
				console.error(error)
			}
		}
		const countBreeds = async () => {
			const breeds = await getBreeds();
			console.log(breeds.data);
			const paymentData = breeds.data;
			return res.redirect(paymentData.redirect_url);
		}
		countBreeds()
	});
})

app.post('/callback', (req, res) => {
	res.redirect('https://makkajperfumes.com/paymentsuccess');
}
);


//-----------------------------------------------------------port--------------------------------------------------------------------
const port = process.env.PORT || 5000;

server.listen(port, (req, res) => {
	console.log('server start at ' + port);
});

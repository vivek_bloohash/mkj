var express = require("express");
const router = express.Router();
const Order = require("../../modal/orderschema");
const Cart = require("../../modal/cartschema");
const Address = require("../../modal/address");
const Razorpay = require("razorpay");
const { response } = require("express");
const nodeCCAvenue = require("node-ccavenue");
const https = require("https");
const querystring = require("querystring");
const axios = require("axios");
const reques = require("request");
const nodemailer = require("nodemailer");
var path = require('path');
var fs = require('fs');
var handlebars = require('handlebars');
//----------------------------------------------------post makaj Order api------------------------------------------------------------------------

router.post("/order", (req, res) => {
  const userId = req.body.userId;
  console.log(req.body);

  try {
    Cart.find({ userId: userId }, (err, usercart) => {
      if (err) {
        return res.status(500).send({ err: err });
      }
      Address.find({ userId: userId }, (err, user) => {
        if (err) {
          return res.status(500).send({ err: err });
        }
        Order.insertMany(
          {
            userId: userId,
            orderItems: usercart[0]?.products,
            shippingAddress: user[0]?.shippingAddress,
            paymentMethod: req.body?.paymentMethod,
            paymentResult: req.body?.paymentResult,
            taxPrice: req.body?.taxPrice,
            shippingPrice: req.body?.shippingPrice,
            totalPrice: req.body?.totalPrice,
            transactionId: req.body?.transactionId,
            isPaid: req.body?.isPaid,
            paidAt: req.body?.paidAt,
            isDelivered: false,
            isCancelled: false,
          },
          (err, users) => {
            if (err) {
              console.log(err);

              return res.status(400).send({ err: err });
            }

            const date = new Date();
            const headers = {
              'Access-Control-Allow-Origin': '*',
              'Content-Type': 'application/json',
              'Cache-Control': 'no-cache',
              'Pragma': 'no-cache'
            }

            console.log(users)
            if (users) {

              axios
                .post("http://track.smsaexpress.com/SecomRestWebApi/api/addship", {
                  "passkey": "ApT@4725",
                  "refno": date.getTime(),
                  "cPOBox": users[0]?.shippingAddress?.pin,
                  "cName": users[0]?.shippingAddress?.firstname + " " + users[0]?.shippingAddress?.lastname,
                  "cntry": users[0]?.shippingAddress?.country,
                  "cCity": users[0]?.shippingAddress?.city,
                  "cZip": users[0]?.shippingAddress?.pin,
                  "cMobile": users[0]?.shippingAddress?.phone,
                  "cTel1": users[0]?.shippingAddress?.phone,
                  "cTel2": users[0]?.shippingAddress?.phone,
                  "PCs": users[0]?.orderItems?.length,
                  "cAddr1": users[0]?.shippingAddress?.address + " " + users[0]?.shippingAddress?.appartment + " " + users[0]?.shippingAddress?.city + " " + users[0]?.shippingAddress?.state + " " + users[0]?.shippingAddress?.country,
                  "cAddr2": users[0]?.shippingAddress?.address + " " + users[0]?.shippingAddress?.appartment + " " + users[0]?.shippingAddress?.city + " " + users[0]?.shippingAddress?.state + " " + users[0]?.shippingAddress?.country,
                  "shipType": "DLV",
                  "idNo": date.getTime(),
                  "cEmail": users[0]?.shippingAddress?.email,
                  "carrValue": users[0]?.totalPrice,
                  "carrCurr": users[0]?.orderItems[0]?.pricecon,
                  "codAmt": users[0]?.totalPrice,
                  "weight": users[0]?.orderItems[0]?.weight,
                  "itemDesc": users[0]?.orderItems[0]?.description,
                  "custVal": users[0]?.totalPrice,
                  "custCurr": users[0]?.totalPrice,
                  "insrAmt": users[0]?.totalPrice,
                  "insrCurr": users[0]?.orderItems[0]?.pricecon,
                  "sName": users[0]?.shippingAddress?.firstname + " " + users[0]?.shippingAddress?.lastname,
                  "sContact": users[0]?.shippingAddress?.phone,
                  "sAddr1": users[0]?.shippingAddress?.address + " " + users[0]?.shippingAddress?.appartment + " " + users[0]?.shippingAddress?.city + " " + users[0]?.shippingAddress?.state + " " + users[0]?.shippingAddress?.country,
                  "sAddr2": users[0]?.shippingAddress?.address + " " + users[0]?.shippingAddress?.appartment + " " + users[0]?.shippingAddress?.city + " " + users[0]?.shippingAddress?.state + " " + users[0]?.shippingAddress?.country,
                  "sCity": users[0]?.shippingAddress?.city,
                  "sPhone": users[0]?.shippingAddress?.phone,
                  "sCntry": users[0]?.shippingAddress?.country,
                  "sentDate": date.getTime(),
                  "prefDelvDate": "3",
                  "gpsPoints": "123",
                }, { headers: headers })
                .then((res) => {
                  console.log(res.data);
                  Order.updateOne({ _id: users[0]?._id }, {
                    $set: {
                      awb: res?.data
                    }
                  }
                    , (err, update) => {
                      if (err) {
                        console.log(err);

                        return res.status(500).send({ err: err });
                      }
                      console.log(update)
                    }
                  )
                }).catch((err) => {
                  console.log(err)
                });
            }


            return res.status(200).send(users);
          }
        );
      });
    });

  }
  catch (err) {
    console.log(err)
  }

});

router.get("/order/:userId", (req, res) => {
  Order.find(
    { userId: req.params.userId, isDelivered: false },

    (err, user) => {
      if (err) {
        return res.status(500).send({ err: err });
      }
      console.log(user)
      const headers = {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache',
        'Pragma': 'no-cache'
      }

      const newDate = new Date(res?.data?.Tracking[0]?.Date);

      for (let i = 0; i < user.length; i++) {
        if (user[i].awb) {
          axios.get(`http://track.smsaexpress.com/SecomRestWebApi/api/getTracking?awbNo=${user[i].awb}&passKey=ApT@4725`, { headers: headers }).then((res) => {
            console.log(res);
            if (res?.data?.Tracking[0]?.Activity == "PROOF OF DELIVERY CAPTURED") {
              Order.updateOne({ _id: user[i]?._id }, {
                $set: {
                  activity: res?.data?.Tracking[0]?.Activity,
                  isDelivered: true,
                  deliveredAt: newDate
                }
              }, (err, userss) => {
                if (err) {
                  return res.status(500).send({ err: err });
                }

              })
            }
            else {
              Order.updateOne({ _id: user[i]?._id }, {
                $set: {
                  activity: res?.data?.Tracking[0]?.Activity
                }
              }, (err, userss) => {
                if (err) {
                  return res.status(500).send({ err: err });
                }

              })
            }
          }).catch((err) => {
            console.log(err)
          })
        }
      }
      return res.status(200).send({ user: user });
    }
  );
});

router.post("/order/delivered", async (req, res) => {
  try {
    let order = await Order.findOne({ _id: req.body.id });

    if (order) {
      //cart exists for user

      //product does not exists in cart, add new item
      order.isDelivered = true;

      order = await order.save();
      console.log(order);
      return res.status(201).send(order);
    }
  } catch (err) {
    console.log(err);
    res.status(500).send("Something went wrong");
  }
});

router.get("/orders/:id", (req, res) => {
  Order.find({ _id: req.params.id }, (err, user) => {
    if (err) {
      return res.status(500).send({ err: err });
    }

    const headers = {
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache',
      'Pragma': 'no-cache'
    }

    const newDate = new Date(res?.data?.Tracking[0]?.Date);

    for (let i = 0; i < user.length; i++) {
      if (user[i].awb) {
        axios.get(`http://track.smsaexpress.com/SecomRestWebApi/api/getTracking?awbNo=${user[i].awb}&passKey=ApT@4725`, { headers: headers }).then((res) => {
          console.log(res);
          if (res?.data?.Tracking[0]?.Activity == "PROOF OF DELIVERY CAPTURED") {
            Order.updateOne({ _id: user[i]?._id }, {
              $set: {
                activity: res?.data?.Tracking[0]?.Activity,
                isDelivered: true,
                deliveredAt: newDate
              }
            }, (err, userss) => {
              if (err) {
                return res.status(500).send({ err: err });
              }

            })
          }
          else {
            Order.updateOne({ _id: user[i]?._id }, {
              $set: {
                activity: res?.data?.Tracking[0]?.Activity
              }
            }, (err, userss) => {
              if (err) {
                return res.status(500).send({ err: err });
              }

            })
          }
        }).catch((err) => {
          console.log(err)
        })
      }
    }

    return res.status(200).send({ user: user });
  });
});

router.get("/allorder/:userId", (req, res) => {
  Order.find(
    { userId: req.params.userId },
    [
      "_id",
      "userId",
      "orderItems",
      "shippingAddress",
      "paymentMethod",
      "paymentResult",
      "taxPrice",
      "shippingPrice",
      "totalPrice",
      "deliveryInstruction",
      "transactionId",
      "isPaid",
      "paidAt",
      "isDelivered",
      "isCancelled",
      "cancelReason",
      "deliveredAt",
      "createdAt",
      "activity",
      "awb"
    ],
    {
      sort: {
        createdAt: -1, //Sor't by Date Added DESC
      },
    },
    (err, user) => {
      if (err) {
        return res.status(500).send({ err: err });
      }
      const headers = {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache',
        'Pragma': 'no-cache'
      }

      const newDate = new Date(res?.data?.Tracking[0]?.Date);

      for (let i = 0; i < user.length; i++) {
        if (user[i].awb) {
          axios.get(`http://track.smsaexpress.com/SecomRestWebApi/api/getTracking?awbNo=${user[i].awb}&passKey=ApT@4725`, { headers: headers }).then((res) => {
            console.log(res);
            if (res?.data?.Tracking[0]?.Activity == "PROOF OF DELIVERY CAPTURED") {
              Order.updateOne({ _id: user[i]?._id }, {
                $set: {
                  activity: res?.data?.Tracking[0]?.Activity,
                  isDelivered: true,
                  deliveredAt: newDate
                }
              }, (err, userss) => {
                if (err) {
                  return res.status(500).send({ err: err });
                }

              })
            }
            else {
              Order.updateOne({ _id: user[i]?._id }, {
                $set: {
                  activity: res?.data?.Tracking[0]?.Activity
                }
              }, (err, userss) => {
                if (err) {
                  return res.status(500).send({ err: err });
                }

              })
            }
          }).catch((err) => {
            console.log(err)
          })
        }
      }

      console.log(user);
      return res.status(200).send({ user: user });
    }
  );
});

router.get("/adminorder", (req, res) => {
  Order.find({}, (err, user) => {
    if (err) {
      return res.status(500).send({ err: err });
    }
    const headers = {
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache',
      'Pragma': 'no-cache'
    }

    const newDate = new Date(res?.data?.Tracking[0]?.Date);

    for (let i = 0; i < user.length; i++) {
      if (user[i].awb) {
        axios.get(`http://track.smsaexpress.com/SecomRestWebApi/api/getTracking?awbNo=${user[i].awb}&passKey=ApT@4725`, { headers: headers }).then((res) => {
          console.log(res);
          if (res?.data?.Tracking[0]?.Activity == "PROOF OF DELIVERY CAPTURED") {
            Order.updateOne({ _id: user[i]?._id }, {
              $set: {
                activity: res?.data?.Tracking[0]?.Activity,
                isDelivered: true,
                deliveredAt: newDate
              }
            }, (err, userss) => {
              if (err) {
                return res.status(500).send({ err: err });
              }

            })
          }
          else {
            Order.updateOne({ _id: user[i]?._id }, {
              $set: {
                activity: res?.data?.Tracking[0]?.Activity
              }
            }, (err, userss) => {
              if (err) {
                return res.status(500).send({ err: err });
              }

            })
          }
        }).catch((err) => {
          console.log(err)
        })
      }
    }
    return res.status(200).send({ user: user });
  });
});

router.post("/postmail", (req, res) => {
  console.log(req.body.email);
  const filePath = path.join(__dirname, '../../utils/order.html');
  const source = fs.readFileSync(filePath, 'utf-8').toString();
  const template = handlebars.compile(source);
  const date = new Date();
  const replacements = {
    date: date.getMonth() + 1 + "-" + date.getDate() + "-" + date.getFullYear()
  };
  const htmlToSend = template(replacements);

  sendEmail(req.body.email, "Order Confirmation", "confirmend", htmlToSend);
  return res.status(200);
});

const sendEmail = async (email, subject, text, html) => {
  try {
    const transporter = nodemailer.createTransport({
      service: "Outlook365",
      host: "smtp.office365.com",
      port: "587",
      auth: {
        user: "info@makkaj.com",
        pass: "Bol786$mj",
      },
    })
      ;

    await transporter.sendMail({
      from: "info@makkaj.com",
      to: email,
      subject: subject,
      text: text,
      html: html
    });

    console.log("email sent sucessfully");
  } catch (error) {
    console.log(error, "email not sent");
  }
};

router.post("/checkout", function (req, res) {
  let entityid = "";
  if (req.body.crr == "SAR") {
    entityid = "8acda4c77f25f8b5017f4a92578836a5";
  } else if (req.body.crr == "AED") {
    entityid = "8acda4c77f25f8b5017f4a9198a73694";
  }
  console.log(req.body);
  request(
    function (responseData) {
      console.log(responseData.result);
      res.json(responseData);
    },
    req.body.amt,
    req.body.crr,
    req.body.address,
    entityid
  );
});

function request(callback, amt, crr, address, entityid) {
  let z = new Date();
  const generateId = () =>
    Math.floor(100000000 + Math.random() * 900000000).toString();

  let id = generateId();
  var path = "/v1/checkouts";
  var data = querystring.stringify({
    entityId: entityid,
    amount: parseInt(amt),
    currency: "SAR",
    paymentType: "DB",
    merchantTransactionId: id,
    "customer.email": address.shippingAddress.email,
    "customer.givenName": address.shippingAddress.firstname,
    "customer.surname": address.shippingAddress.lastname,
    "billing.street1": address.shippingAddress.address,
    "billing.city": address.shippingAddress.city,
    "billing.state": address.shippingAddress.state,
    "billing.country": crr == "SAR" ? "AE" : "SA",
    "billing.postcode": address.shippingAddress.pin,
  });
  console.log(data);
  var options = {
    port: 443,
    host: "oppwa.com",
    path: path,
    method: "POST",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      "Content-Length": data.length,
      Authorization:
        "Bearer OGFjZGE0Yzc3ZjI1ZjhiNTAxN2Y0YTkwYTI1MTM2ODd8RE1yRHBDTm05UQ==",
    },
  };
  var postRequest = https.request(options, function (res) {
    res.setEncoding("utf8");
    res.on("data", function (chunk) {
      jsonRes = JSON.parse(chunk);
      return callback(jsonRes);
    });
  });
  postRequest.write(data);
  postRequest.end();
}

router.post("/result", function (req, res) {
  console.log("---------", req.body);
  resultRequest(
    req.body.resourcePath,
    req.body.entityid,
    function (responseData) {
      res.json(responseData);
    }
  );
});

function resultRequest(resourcePath, entityid, callback) {
  console.log(entityid);
  var path = resourcePath;
  path += `?entityId=${entityid}`;
  console.log(path);
  //   var options = {
  //     port: 443,
  //     host: 'test.oppwa.com',
  //     path: path,
  //     method: 'GET',
  //     headers: {
  //       Authorization:
  //         'Bearer OGE4Mjk0MTc1MDYwODIzYTAxNTA2MDg2NmE0ODAwMmN8WlI5eld5UlA=',
  //     },
  //   };
  //   var postRequest = https.request(options, function(res) {
  //     res.setEncoding('utf8');
  //     res.on('data', function(chunk) {
  //       jsonRes = JSON.parse(chunk);
  //       return callback(jsonRes);
  //     });
  //   });
  //   postRequest.end();
  const url = "https://oppwa.com" + path;
  axios
    .get(url, {
      headers: {
        Authorization:
          "Bearer OGFjZGE0Yzc3ZjI1ZjhiNTAxN2Y0YTkwYTI1MTM2ODd8RE1yRHBDTm05UQ==",
      },
    })
    .then(function (response) {
      // handle success
      // console.log(response);
      try {
        resDate = JSON.parse(response);
      } catch (e) {
        resData = response;
        console.log(e.result);
        // console.log(resData.data.id);
      }

      return callback(resData.data);
    })
    .catch(function (error) {
      // handle error
      //

      console.log(error.result);
    });
}


const instance = new Razorpay({
  key_id: "rzp_test_V9McTPCQ0fi6X7",
  key_secret: "R3n6qAABuX0cJn0vLOyZo8XX",
});

router.get("/payment/:curr/:amt", (req, res) => {
  try {
    const options = {
      amount: req.params.amt * 100, // amount == Rs 10
      currency: req.params.curr,
      receipt: "receipt#1",
      payment_capture: 0,
      // 1 for automatic capture // 0 for manual capture
    };
    instance.orders.create(options, async function (err, order) {
      if (err) {
        return res.status(500).json({
          message: "Something Went Wrong",
        });
      }
      return res.status(200).json(order);
    });
  } catch (err) {
    return res.status(500).json({
      message: "Something Went Wrong",
    });
  }
});

router.post("/capture/:paymentId", (req, res) => {
  try {
    return reques(
      {
        method: "POST",
        url: `https://rzp_test_V9McTPCQ0fi6X7:R3n6qAABuX0cJn0vLOyZo8XX@api.razorpay.com/v1/payments/${req.params.paymentId}/capture`,
        form: {
          amount: req.body.amt * 100, // amount == Rs 10 // Same As Order amount
          currency: req.body.crr,
        },
      },
      async function (err, response, body) {
        if (err) {
          return res.status(500).json({
            message: "Something Went Wrong",
          });
        }

        return res.status(200).json({ body: body, success: true });
      }
    );
  } catch (err) {
    console.log(err);
    return res.status(500).json({
      message: "Something Went Wrong",
    });
  }
});




module.exports = router;

import React, { useEffect, useState } from "react";
import "./privacypolicy.css";
import axios from "axios";
import logo from "../../assets/images/crest-large.png";
import { useDispatch, useSelector } from "react-redux";
import { listPrice } from "../../actions/productaction";
import { Translate } from "react-auto-translate";
import { useLayoutEffect } from "react";


export const Privacypolicy = () => {
  const [datas, setdatas] = useState(null);
  const dispatch = useDispatch();

  useEffect(async () => {
    const { data } = await axios.get(`https://api.makkajperfumes.com/api/privacy`);
    setdatas(data.user[0]);
  }, []);
  const prices = useSelector((state) => state.priceList);
  const { price } = prices;

  useEffect(() => {
    dispatch(listPrice());
  }, [dispatch]);

  useLayoutEffect(() => {
    window.scrollTo(0, 0)
  });
  return localStorage.getItem("translate") == "en" && datas != null ? (
    <div className="aboutus-container">
      <div className="aboutus-logo">
        <hr />
        <img src={logo} />
        <hr />
      </div>
      <div className="aboutus-head">
        <h1><Translate>PRIVACY POLICY</Translate></h1>
      </div>
      <div className="aboutus-container-box">
        <div className="about-para">
          <p><Translate>Al Majeed Modern Industry for Perfume Mfg. & Oudh Processing with brand name as Makkaj Perfumes an organization incorporated and located in Makkah, KSA and (who we refer to as "we", "us" and "our" below), take your privacy very seriously. This Privacy Statement explains what personal information we collect, how and when it is collected, what we use it for now and how we will use it in the FUTURE and details of the circumstances in which we may disclose it to third parties. If you have any questions about the way in which your information is being collected or used which are not answered by this Privacy Statement and/or any complaints please contact us on estore@makkaj.com<br></br><br></br>
            By visiting and using www.makkajperfumes.com (the "Website") or any other application or website ("Application") for the purchase or sampling of products from Makkaj Perfumes (as applicable) you acknowledge that you have read, understood and are consenting to the practices in relation to use and disclosure of your personal information described in this Privacy Statement and our Terms and Conditions. Please obtain your parent's or guardian's consent before providing us with any personal information if you are visiting and using the Site; Anywhere else in the world and you are under the age of 16.
          </Translate></p>
        </div>

        <div className="about-us-decor">

          <div>
            <h5><Translate>WHAT INFORMATION DO WE COLLECT AND HOW WILL WE USE IT?
            </Translate></h5>
            <p><Translate>When you REGISTER for an account with us, place an order with us or send us an enquiry, we will collect certain personal information from you, for example, your name, postal address, phone numbers, e-mail addresses. We may also obtain information about you as a result of authentication or identity checks. We use this information to identify you as a customer, to process your order, to deliver products, to process payments, to update our records, to enable your use of interactive features of our service and to generally manage your account with us and if you have consented to provide you with information by post, e-mail, mobile messaging, telephone communications and/or through any other electronic means including social network platforms about our products, events, promotions and services. We may also use this information to tailor how our website appears to you and to tailor the contents of our communications with you, so as to make the website and those communications more relevant to you.<br></br><br></br>
              We may also at times ask you for other information, for example product and category preferences, age and any special dates (such as birthday and anniversary) which will be used to enhance our service to you. Providing us with this sort of information is entirely voluntary, however we may not be able to process your order if you do not provide us with all the requested information.
              We may also use your personal information for our internal MARKETING and demographic studies, together with non-personal data to analyze, profile and monitor customer patterns so we can consistently improve our products and services and understand what may be of interest to you and other customers.
            </Translate></p>
          </div>
        </div>
        <div className="about-us-decor abooutus-images">
          <div>
            <h5><Translate>RETENTION AND DELETION OF PERSONAL INFORMATION</Translate></h5>
            <p><Translate>We shall only keep your information as long as you remain an active customer. If you wish to request the deletion of your personal details, or wish to change your preferences at any time please contact our Customer Service Team.</Translate></p>
          </div>

        </div>
        <div className="about-us-decor">

          <div>
            <h5><Translate>WHAT YOU CAN EXPECT FROM MAKKAJ PERFUMES
            </Translate></h5>
            <p><Translate>We will at all times seek to comply with the requirements of the Act to ensure that the personal information you give us is kept appropriately secure and processed fairly and lawfully.
            </Translate></p>
          </div>
        </div>
        <div className="about-us-decor abooutus-images">
          <div>
            <h5><Translate>OTHER PEOPLE WHO MIGHT USE YOUR INFORMATION
            </Translate></h5>
            <p><Translate>“All credit/debit cards details and personally identifiable information will NOT be stored, sold, shared, rented or leased to any third parties”.<br></br><br></br>
              You also acknowledge and agree that in certain circumstances we may disclose personal information relating to you to third parties, for example, in order to conform to any requirements of law, to comply with any legal process, for the purposes of obtaining legal advice, for the purposes of CREDIT RISK reduction, to prevent and detect fraud and/or to protect and defend the rights and property of MAKKAJ Perfumes. At all times where we disclose your information for the purposes of credit risk reduction and fraud prevention we will take all steps reasonably necessary to ensure that it remains secure.
            </Translate></p>
          </div>

        </div>
        <div className="about-us-decor abooutus-images">
          <div>
            <h5><Translate>UPDATING AND REVIEWING YOUR PERSONAL DETAILS
            </Translate></h5>
            <p><Translate>You can amend or update your information by logging into My ACCOUNT on the Website and amending your details as appropriate.
            </Translate></p>
          </div>

        </div>
        <div className="about-us-decor abooutus-images">
          <div>
            <h5><Translate>CHANGES TO THIS PRIVACY STATEMENT
            </Translate></h5>
            <p><Translate>We may update this Privacy Statement from time to time. The amended Privacy Statement will be posted on the Website. Please check this page regularly for changes to this Privacy Statement. If you continue to use the Website or if you submit information to us following such changes, you will be deemed to have read and agreed them.
            </Translate></p>
          </div>

        </div>
        <div className="about-us-decor abooutus-images">
          <div>
            <h5><Translate>THIRD PARTY LINKS
            </Translate></h5>
            <p><Translate>The Website and/or Applications may from time to time contain links to other websites not controlled by us. We do not accept responsibility or LIABILITY for the privacy practices, data collected or the content of such other websites. The operators of these linked websites are not under a duty to abide by this Privacy Statement. If there are terms and conditions, other privacy statements or policies appearing on those websites, you should also review them carefully as your use of those sites may be subject to them.
            </Translate></p>
          </div>

        </div>
        <div className="about-us-decor abooutus-images">
          <div>
            <h5><Translate>SECURITY STATEMENT
            </Translate></h5>
            <p><Translate>Unfortunately transmission of information via the Internet is not completely secure. Although we do our best to protect your personal data we cannot guarantee the security of your data submitted to us and any transmission is at your own risk.<br></br><br></br>
              We do however use strict procedures and security features to try to prevent unauthorized access wherever possible. Personal information provided to MAKKAJ Perfumes via the Website or via any Applications and online CREDIT card transactions are transmitted through a secure server using Secure Socket Layering (SSL), encryption technology. When the letters "http" in the URL change to "https," the "s" indicates you are in a secure area EMPLOYING SSL; also, your browser may give you a pop-up message that you are about to enter a secure area or display a padlock image.<br></br><br></br>
              The Website and the Applications use this encryption technology to protect your information during data transport. SSL encrypts ordering information such as your name, address and credit card number. Our customer service center and stores also operate over a private, secure network. Please note that e-mail is not encrypted and is not considered to be a secure means of transmitting credit card information.<br></br><br></br>For any further queries, please contact us.
            </Translate></p>
          </div>

        </div>

      </div>
    </div>
  ) : datas != null ? (
    <div className="aboutus-container">
      <div className="aboutus-logo">
        <hr />
        <img src={logo} />
        <hr />
      </div>
      <div className="aboutus-head">
      <h1><Translate>PRIVACY POLICY</Translate></h1>
      </div>
      <div className="aboutus-container-box">
        <div className="about-para">
          <p><Translate>Al Majeed Modern Industry for Perfume Mfg. & Oudh Processing with brand name as Makkaj Perfumes an organization incorporated and located in Makkah, KSA and (who we refer to as "we", "us" and "our" below), take your privacy very seriously. This Privacy Statement explains what personal information we collect, how and when it is collected, what we use it for now and how we will use it in the FUTURE and details of the circumstances in which we may disclose it to third parties. If you have any questions about the way in which your information is being collected or used which are not answered by this Privacy Statement and/or any complaints please contact us on estore@makkaj.com<br></br><br></br>
            By visiting and using www.makkajperfumes.com (the "Website") or any other application or website ("Application") for the purchase or sampling of products from Makkaj Perfumes (as applicable) you acknowledge that you have read, understood and are consenting to the practices in relation to use and disclosure of your personal information described in this Privacy Statement and our Terms and Conditions. Please obtain your parent's or guardian's consent before providing us with any personal information if you are visiting and using the Site; Anywhere else in the world and you are under the age of 16.
          </Translate></p>
        </div>

        <div className="about-us-decor">

          <div>
            <h5><Translate>WHAT INFORMATION DO WE COLLECT AND HOW WILL WE USE IT?
            </Translate></h5>
            <p><Translate>When you REGISTER for an account with us, place an order with us or send us an enquiry, we will collect certain personal information from you, for example, your name, postal address, phone numbers, e-mail addresses. We may also obtain information about you as a result of authentication or identity checks. We use this information to identify you as a customer, to process your order, to deliver products, to process payments, to update our records, to enable your use of interactive features of our service and to generally manage your account with us and if you have consented to provide you with information by post, e-mail, mobile messaging, telephone communications and/or through any other electronic means including social network platforms about our products, events, promotions and services. We may also use this information to tailor how our website appears to you and to tailor the contents of our communications with you, so as to make the website and those communications more relevant to you.<br></br><br></br>
              We may also at times ask you for other information, for example product and category preferences, age and any special dates (such as birthday and anniversary) which will be used to enhance our service to you. Providing us with this sort of information is entirely voluntary, however we may not be able to process your order if you do not provide us with all the requested information.
              We may also use your personal information for our internal MARKETING and demographic studies, together with non-personal data to analyze, profile and monitor customer patterns so we can consistently improve our products and services and understand what may be of interest to you and other customers.
            </Translate></p>
          </div>
        </div>
        <div className="about-us-decor abooutus-images">
          <div>
            <h5><Translate>RETENTION AND DELETION OF PERSONAL INFORMATION</Translate></h5>
            <p><Translate>We shall only keep your information as long as you remain an active customer. If you wish to request the deletion of your personal details, or wish to change your preferences at any time please contact our Customer Service Team.</Translate></p>
          </div>

        </div>
        <div className="about-us-decor">

          <div>
            <h5><Translate>WHAT YOU CAN EXPECT FROM MAKKAJ PERFUMES
            </Translate></h5>
            <p><Translate>We will at all times seek to comply with the requirements of the Act to ensure that the personal information you give us is kept appropriately secure and processed fairly and lawfully.
            </Translate></p>
          </div>
        </div>
        <div className="about-us-decor abooutus-images">
          <div>
            <h5><Translate>OTHER PEOPLE WHO MIGHT USE YOUR INFORMATION
            </Translate></h5>
            <p><Translate>“All credit/debit cards details and personally identifiable information will NOT be stored, sold, shared, rented or leased to any third parties”.<br></br><br></br>
              You also acknowledge and agree that in certain circumstances we may disclose personal information relating to you to third parties, for example, in order to conform to any requirements of law, to comply with any legal process, for the purposes of obtaining legal advice, for the purposes of CREDIT RISK reduction, to prevent and detect fraud and/or to protect and defend the rights and property of MAKKAJ Perfumes. At all times where we disclose your information for the purposes of credit risk reduction and fraud prevention we will take all steps reasonably necessary to ensure that it remains secure.
            </Translate></p>
          </div>

        </div>
        <div className="about-us-decor abooutus-images">
          <div>
            <h5><Translate>UPDATING AND REVIEWING YOUR PERSONAL DETAILS
            </Translate></h5>
            <p><Translate>You can amend or update your information by logging into My ACCOUNT on the Website and amending your details as appropriate.
            </Translate></p>
          </div>

        </div>
        <div className="about-us-decor abooutus-images">
          <div>
            <h5><Translate>CHANGES TO THIS PRIVACY STATEMENT
            </Translate></h5>
            <p><Translate>We may update this Privacy Statement from time to time. The amended Privacy Statement will be posted on the Website. Please check this page regularly for changes to this Privacy Statement. If you continue to use the Website or if you submit information to us following such changes, you will be deemed to have read and agreed them.
            </Translate></p>
          </div>

        </div>
        <div className="about-us-decor abooutus-images">
          <div>
            <h5><Translate>THIRD PARTY LINKS
            </Translate></h5>
            <p><Translate>The Website and/or Applications may from time to time contain links to other websites not controlled by us. We do not accept responsibility or LIABILITY for the privacy practices, data collected or the content of such other websites. The operators of these linked websites are not under a duty to abide by this Privacy Statement. If there are terms and conditions, other privacy statements or policies appearing on those websites, you should also review them carefully as your use of those sites may be subject to them.
            </Translate></p>
          </div>

        </div>
        <div className="about-us-decor abooutus-images">
          <div>
            <h5><Translate>SECURITY STATEMENT
            </Translate></h5>
            <p><Translate>Unfortunately transmission of information via the Internet is not completely secure. Although we do our best to protect your personal data we cannot guarantee the security of your data submitted to us and any transmission is at your own risk.<br></br><br></br>
              We do however use strict procedures and security features to try to prevent unauthorized access wherever possible. Personal information provided to MAKKAJ Perfumes via the Website or via any Applications and online CREDIT card transactions are transmitted through a secure server using Secure Socket Layering (SSL), encryption technology. When the letters "http" in the URL change to "https," the "s" indicates you are in a secure area EMPLOYING SSL; also, your browser may give you a pop-up message that you are about to enter a secure area or display a padlock image.<br></br><br></br>
              The Website and the Applications use this encryption technology to protect your information during data transport. SSL encrypts ordering information such as your name, address and credit card number. Our customer service center and stores also operate over a private, secure network. Please note that e-mail is not encrypted and is not considered to be a secure means of transmitting credit card information.<br></br><br></br>For any further queries, please contact us.
            </Translate></p>
          </div>

        </div>

      </div>
    </div>
  ) : (<div></div>)
}

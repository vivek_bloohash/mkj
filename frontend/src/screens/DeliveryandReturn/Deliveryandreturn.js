import React, { useEffect, useState } from "react";
import "./Deliveryandreturn.css";
import logo from "../../assets/images/crest-large.png";

import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { listPrice } from "../../actions/productaction";
import { Translate } from "react-auto-translate";
import { useLayoutEffect } from "react";

export const Deliveryandreturn = () => {
  const [datas, setdatas] = useState(null);
  const dispatch = useDispatch();

  useEffect(async () => {
    const { data } = await axios.get(`https://api.makkajperfumes.com/api/return`);
    setdatas(data.user[0]);
  }, []);
  const prices = useSelector((state) => state.priceList);
  const { price } = prices;

  useEffect(() => {
    dispatch(listPrice());
  }, [dispatch]);

  useLayoutEffect(() => {
    window.scrollTo(0, 0)
  });

  return localStorage.getItem("translate") == "en" && datas != null ? (
    <div className="aboutus-container">
      <div className="aboutus-logo">
        <hr />
        <img src={logo} />
        <hr />
      </div>
      <div className="aboutus-head">
        <h1><Translate>RETURNS & CANCELLATIONS
        </Translate></h1>
      </div>
      <div className="aboutus-container-box">
        <div className="about-us-decor">

          <div>
            <h5><Translate>RETURN POLICY
            </Translate></h5>
            <p>
              <ol><Translate>
                <li>All items are subject to return except for: Agar/wood, D/Oudh and Attars in loose forms.</li>
                <li>Return is accepted if product is with original packaging.</li>
                <li>The submissions of a return request have to be within seven (7) days from the date of delivery.</li>
                <li>In case of damaged or pilferage items the submissions of a return request have to be within seven (7) days from the date of delivery.</li>
                <li>The submissions of a return request for items purchased on offer is within seven (7) days from the date of delivery.</li>
                <li>Any Complimentary gifts which come along with purchase needs to be returned</li>
                <li>Return Shipping charges will be borne by the customer except for damaged or pilferage items.</li>
                <li>Original invoice is required for the return.</li>
              </Translate></ol>
            </p>
          </div>
        </div>
        <div className="about-us-decor abooutus-images">
          <div>
            <h5><Translate>HOW TO RETURN YOUR PURCHASE</Translate></h5>
            <p><Translate>To submit a return request, please email: estore@makkaj.com, with your order number and description of the issue you faced.</Translate></p>
          </div>

        </div>
        <div className="about-us-decor">

          <div>
            <h5><Translate>RECEIVING A REFUND
            </Translate></h5>
            <p><Translate>
              Refund will be made to the customer after receiving the returned goods within 10-15 working days via credit card or bank transfer.
              Business Days:<br></br><br></br>
              SAUDI ARABIA – Sunday to Thursday.<br></br><br></br>
              INDIA – Monday to Friday.<br></br><br></br>
              UAE – Monday to Friday.<br></br><br></br>
              Except national holidays.<br></br><br></br>

            </Translate></p>
          </div>
        </div>
        <div className="about-us-decor abooutus-images">
          <div>
            <h5><Translate>EXCHANGES
            </Translate></h5>
            <p><Translate>Currently orders cannot be exchanged online.
            </Translate></p>
          </div>

        </div>
        <div className="about-us-decor abooutus-images">
          <div>
            <h5><Translate>CANCELLATIONS
            </Translate></h5>
            <p><Translate>In certain circumstances, before your order has been dispatched, we may be able to cancel your order. If you wish to cancel your order, then you’re required to inform Makkaj Perfumes through email or telephone about your cancellation within 24 hours from the time of order being placed.<br></br><br></br>
              Please note, we are unable to combine orders, change the items, edit your billing and shipping details or add pieces to an existing order once it has been placed.<br></br><br></br>
              Your order will be processed and, subject to being accepted by us, a confirmation email will be sent upon shipping.<br></br><br></br>
              For any further queries, please contact us
            </Translate></p>
          </div>

        </div>
      </div>
    </div>
  ) : datas != null ? (
    <div className="aboutus-container">
      <div className="aboutus-logo">
        <hr />
        <img src={logo} />
        <hr />
      </div>
      <div className="aboutus-head">
        <h1>{datas.head.AED}</h1>
      </div>
      <div className="aboutus-container-box">
        <div className="about-us-decor">

          <div>
            <h5><Translate>RETURN POLICY
            </Translate></h5>
            <p>
              <ol><Translate>
                <li>All items are subject to return except for: Agar/wood, D/Oudh and Attars in loose forms.</li>
                <li>Return is accepted if product is with original packaging.</li>
                <li>The submissions of a return request have to be within seven (7) days from the date of delivery.</li>
                <li>In case of damaged or pilferage items the submissions of a return request have to be within seven (7) days from the date of delivery.</li>
                <li>The submissions of a return request for items purchased on offer is within seven (7) days from the date of delivery.</li>
                <li>Any Complimentary gifts which come along with purchase needs to be returned</li>
                <li>Return Shipping charges will be borne by the customer except for damaged or pilferage items.</li>
                <li>Original invoice is required for the return.</li>
              </Translate></ol>
            </p>
          </div>
        </div>
        <div className="about-us-decor abooutus-images">
          <div>
            <h5><Translate>HOW TO RETURN YOUR PURCHASE</Translate></h5>
            <p><Translate>To submit a return request, please email: estore@makkaj.com, with your order number and description of the issue you faced.</Translate></p>
          </div>

        </div>
        <div className="about-us-decor">

          <div>
            <h5><Translate>RECEIVING A REFUND
            </Translate></h5>
            <p><Translate>
              Refund will be made to the customer after receiving the returned goods within 10-15 working days via credit card or bank transfer.
              Business Days:<br></br><br></br>
              SAUDI ARABIA – Sunday to Thursday.<br></br><br></br>
              INDIA – Monday to Friday.<br></br><br></br>
              UAE – Monday to Friday.<br></br><br></br>
              Except national holidays.<br></br><br></br>

            </Translate></p>
          </div>
        </div>
        <div className="about-us-decor abooutus-images">
          <div>
            <h5><Translate>EXCHANGES
            </Translate></h5>
            <p><Translate>Currently orders cannot be exchanged online.
            </Translate></p>
          </div>

        </div>
        <div className="about-us-decor abooutus-images">
          <div>
            <h5><Translate>CANCELLATIONS
            </Translate></h5>
            <p><Translate>In certain circumstances, before your order has been dispatched, we may be able to cancel your order. If you wish to cancel your order, then you’re required to inform Makkaj Perfumes through email or telephone about your cancellation within 24 hours from the time of order being placed.<br></br><br></br>
              Please note, we are unable to combine orders, change the items, edit your billing and shipping details or add pieces to an existing order once it has been placed.<br></br><br></br>
              Your order will be processed and, subject to being accepted by us, a confirmation email will be sent upon shipping.<br></br><br></br>
              For any further queries, please contact us
            </Translate></p>
          </div>

        </div>
      </div>
    </div>
  ) : (<div></div>)
};
import React from "react";
import { useLayoutEffect } from "react";
import { Translate } from "react-auto-translate";
import './Contact.css'
import logo from "../../assets/main-logo.png";

export const Contact = () => {
  useLayoutEffect(() => {
  window.scrollTo(0, 0)
});

  return (
    <div className="contact-back">
      <div className="shipping-top-info" style={{backgroundColor:"black"}}>
        <h3><Translate>CONTACT</Translate></h3>
      </div>
      <div className="contact-cont">
        <div>
          <div className="aboutus-logo contact-logo" style={{padding:"0px"}}>
            <hr />
            <img src={logo} alt="logo" />
            <hr />
          </div>
          <h1 className="contact-head">
          <Translate> Contact us</Translate>
          </h1>
          <p className="contact-para">
          <Translate>  We may have already answered your question in the FAQ page.If not please contact us and we will get back to you as soon as possible
            </Translate> </p>
          <div className="aboutus-logo contact-logo" style={{padding:"0px"}}>
            <hr />
<p><Translate>STORES</Translate></p>
            <hr />
          </div>
          <div className="contact-details-cont">
            <div className="contact-detail">
              <img src="./saudi.jpeg"/>
              <p className="contact-p1">
              <Translate>  JEDDAH - KSA</Translate>
              </p>
              <p className="contact-p2">
              <Translate>  BAB SHARIF – KING ABDUL AZIZ,</Translate><br/>
              <Translate>  ST: NEAR AZIZA HOTEL. </Translate><br/>
              <Translate>  AL BALAD, JEDDAH, SAUDI ARABIA.</Translate>
              </p>
              <p className="contact-p3">
              <Translate>  Tel: +966 12 648 2433</Translate><br/>
              <Translate>  Fax: +966 12 648 2381</Translate><br/>
              <Translate> Mob: +966 50 523 6069</Translate><br/>
              <Translate> Email: <a href="mailto:info@makkaj.com">info@makkaj.com</a></Translate> <br/>              
              <Translate> Web: <a href="http://www.makkajperfumes.com/" target="_blank">http://www.makkajperfumes.com/ </a></Translate> <br/>             
              <Translate> Location: </Translate><a href="https://goo.gl/maps/w9vd785YGT42" target="_blank"> <Translate>https://goo.gl/maps/CcuUVuMGuNUCZshBA</Translate></a>
              </p>
            </div>
            <div className="contact-detail">
              <img src="./global.jpg"/>
              <p className="contact-p1">
              <Translate> GLOBAL VILLAGE</Translate>
              </p>
              <p className="contact-p2">
               <Translate> GLOBAL VILLAGE , INDIAN PAVILION,</Translate><br/>
               <Translate> SHOP NO 58, 59, 62 & 63 , DUBAI , UAE.</Translate>
              </p>
              <p className="contact-p3">
              <Translate>  Tel: 043383881</Translate><br/>
              <Translate> Mob: +971 56 1760567</Translate><br/>
              <Translate> Email: <a href="mailto:info@makkaj.com">info@makkaj.com</a></Translate> <br/>              
              <Translate> Web: <a href="http://www.makkajperfumes.com/" target="_blank">http://www.makkajperfumes.com/ </a></Translate> <br/>             
              <Translate> Location: </Translate><a href="https://g.page/globalvillageuae?share" target="_blank"> <Translate>https://g.page/globalvillageuae?share</Translate></a>
              </p>
            </div>
            <div className="contact-detail">
              <img src="./ajman.jpeg"/>
              <p className="contact-p1">
              <Translate>AJMAN</Translate>
              </p>
              <p className="contact-p2">
                <Translate> GROUND FLOOR , SHOP #10, KENZ HYPERMARKET,</Translate><br/>
                <Translate>AL HAMIDIYA, AJMAN, UAE.</Translate>
                <Translate> P.O Box: 19580</Translate>
              </p>
              <p className="contact-p3">
              <Translate>  Tel: +971 67675707</Translate><br/>
              <Translate> Mob: +971 565097537</Translate><br/>
              <Translate> Email: <a href="mailto:info@makkaj.com">info@makkaj.com</a></Translate> <br/>            
              <Translate> Web: <a href="http://www.makkajperfumes.com/" target="_blank">http://www.makkajperfumes.com/ </a></Translate> <br/>     
              <Translate> Location: </Translate><a href="https://goo.gl/maps/aSMHhrxyzbrWJurn9" target="_blank"> <Translate>https://goo.gl/maps/aSMHhrxyzbrWJurn9</Translate></a>
              </p>
            </div>
            <div className="contact-detail">
              <img src="./image1.jpg"/>
              <p className="contact-p1">
              <Translate>AZIZIYAH</Translate>
              </p>
              <p className="contact-p2">
                <Translate> Shop No. 17, Ground Floor,</Translate><br/>
                <Translate>Zi Al-Majaz Mall, Al aziziyah.</Translate><br/>
                <Translate> Makkah Al-Mukarramah, KSA</Translate>
              </p>
              <p className="contact-p3">
              <Translate>  Tel: 012 528 0180</Translate><br/>
              <Translate> Mob: 055 880 4761</Translate><br/>
              <Translate> Email: <a href="mailto:info@makkaj.com">info@makkaj.com</a></Translate> <br/>      
              <Translate> Web: <a href="http://www.makkajperfumes.com/" target="_blank">http://www.makkajperfumes.com/ </a></Translate> <br/>           
              <Translate> Location: </Translate><a href="https://maps.app.goo.gl/1rQbpFyCx58RdvYc8" target="_blank"> <Translate>https://maps.app.goo.gl/1rQbpFyCx58RdvYc8</Translate></a>
              </p>
            </div>
            <div className="contact-detail">
              <img src="./image2.jpg"/>
              <p className="contact-p1">
              <Translate>UTAYBIYYAH</Translate>
              </p>
              <p className="contact-p2">
               <Translate> Shop No. 3, Ground Floor, </Translate><br/>
                <Translate>, Al- Utaybiyyah Main Street,</Translate><br/>
                <Translate>Al- Utaybiyyah, </Translate>
                <Translate> Makkah Al-Mukarramah, KSA.</Translate>
              </p>
              <p className="contact-p3">
              <Translate>  Tel: 012 542 0770</Translate><br/>
              <Translate> Mob: 055 880 5276</Translate><br/>
              <Translate> Email: <a href="mailto:info@makkaj.com">info@makkaj.com</a></Translate> <br/>     
              <Translate> Web: <a href="http://www.makkajperfumes.com/" target="_blank">http://www.makkajperfumes.com/ </a></Translate> <br/>            
              <Translate> Location: </Translate><a href="https://maps.app.goo.gl/CGYK1bb7Tm3nfcSp9" target="_blank"> <Translate>https://maps.app.goo.gl/CGYK1bb7Tm3nfcSp9</Translate></a>
              </p>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  );
};

import axios from "axios";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { orderPost, shippingaddressget } from "../../actions/orderaction";
import { listPrice } from "../../actions/productaction";
import { Rightinfo } from "../../components/rightinfo/rightinfo";
import "./paymentscreen.css";
import store from "../../store";
import { Link } from "react-router-dom";
import $ from "jquery";
import { setcoupon } from "../../actions/cartactions";
import queryString from "query-string";
import { useHistory } from "react-router";
import { Translate } from "react-auto-translate";

export const PayTabSuccess = (props) => {
    const dispatch = useDispatch();
    const addresses = useSelector((state) => state.shipping);
    const { status } = useSelector((state) => state.order);

    const { address } = addresses;
    const prices = useSelector((state) => state.priceList);
    const { price } = prices;
    const [r, setr] = useState(null);
    const history = useHistory();
    const coupons = useSelector((state) => state.getcoupon);
    const { coupon } = coupons;

    useEffect(() => {
        if (JSON.parse(localStorage.getItem("login")) == true) {
            dispatch(
                shippingaddressget(JSON.parse(localStorage.getItem("userInfo")).user)
            );
        } else {
            dispatch(shippingaddressget(localStorage.getItem("guestid")));

        }

        dispatch(listPrice());
    }, [dispatch]);

    useEffect(() => {
        if (coupon != null) {
            localStorage.setItem("coupon", JSON.stringify(coupon));
        }
    }, [coupon]);


    localStorage.setItem("curr", price);

    useEffect(() => {
        if (JSON.parse(localStorage.getItem("login")) == true) {

            store.dispatch(
                orderPost(
                    JSON.parse(localStorage.getItem('userInfo')).user,
                    "Paytab",
                    {
                        id: "",
                        status: "",
                        update_time: "",
                        email_address: ""
                    },
                    localStorage.getItem('tax'),
                    0,
                    localStorage.getItem('total'),
                    "",
                    true,
                    Date.now()
                )
            );
        }
        else {
            store.dispatch(
                orderPost(
                    localStorage.getItem("guestid"),
                    "Paytab",
                    {
                        id: "",
                        status: "",
                        update_time: "",
                        email_address: ""
                    },
                    localStorage.getItem('tax'),
                    0,
                    localStorage.getItem('total'),
                    "",
                    true,
                    Date.now()
                )
            );
        }
        if (localStorage.getItem('coupon') != '') {
            store.dispatch(setcoupon(JSON.parse(localStorage.getItem('coupon'))));
            localStorage.setItem('coupon', '');
        }
        localStorage.setItem('apply', false);



        if (JSON.parse(localStorage.getItem("login")) == true) {

            axios.post("https://api.makkajperfumes.com/api/postmail", {
                email: JSON.parse(localStorage.getItem("userInfo")).email,
            });
        }
        else {
            axios.post("https://api.makkajperfumes.com/api/postmail", {
                email: localStorage.getItem("email"),
            });
        }

    }, []);


    useEffect(() => {
        if (status == true) {
            window.location.replace('/orders');
        }
    }, [status]);

    return (
        <div>
            Payment Successfull
        </div>
    );
};
import React, { Component } from "react";
import queryString from "query-string";
import axios from "axios";
import { orderPost } from "../../../actions/orderaction";
import store from "../../../store";
import { setcoupon } from "../../../actions/cartactions";

class ResultPage extends Component {
  state = {
    responseData: null,
    loading: true,
  };

  componentDidMount() {

    const parsed = queryString.parse(this.props.location.search);
    const resourcePath = parsed.resourcePath;
    let w = "";
    if (localStorage.getItem("curr") == "SAR") {
      w = "8acda4c77f25f8b5017f4a92578836a5";
    }
    if (localStorage.getItem("curr") == "AED") {
      w = "8acda4c77f25f8b5017f4a9198a73694";
    }
    axios

      .post("https://api.makkajperfumes.com/api/result", {
        resourcePath,
        entityid: w,

      })
      .then((res) => {

        this.setState({
          responseData: res.data,
          loading: false,
        });
      });
  }
  checkResult = () => {
    const successPattern = /^(000\.000\.|000\.100\.1|000\.[36])/;
    const manuallPattern = /^(000\.400\.0[^3]|000\.400\.100)/;

    const match1 = successPattern.test(this.state.responseData.result.code);
    const match2 = manuallPattern.test(this.state.responseData.result.code);
    if (match1 || match2) {
      if (JSON.parse(localStorage.getItem("login")) == true) {
        store.dispatch(
          orderPost(
            JSON.parse(localStorage.getItem("userInfo")).user,
            this.state.responseData.paymentType,
            {
              id: this.state.responseData.id,
              status: this.state.responseData.paymentType,
              update_time: Date.now(),
            },
            localStorage.getItem("tax"),
            parseInt(parseFloat(localStorage.getItem('total')) - parseFloat(localStorage.getItem('tax')) > 300 ? 0 : 20),
            parseFloat(localStorage.getItem("total")) + parseInt(parseFloat(localStorage.getItem('total')) - parseFloat(localStorage.getItem('tax')) > 300 ? 0 : 20),
            this.state.responseData.resultDetails.TransactionIdentfier,
            true,
            this.state.responseData.timestamp
          )
        );
      } else {
        store.dispatch(
          orderPost(
            localStorage.getItem("guestid"),
            this.state.responseData.paymentType,
            {
              id: this.state.responseData.id,
              status: this.state.responseData.paymentType,
              update_time: Date.now(),
            },
            localStorage.getItem("tax"),
            parseInt(parseFloat(localStorage.getItem('total')) - parseFloat(localStorage.getItem('tax')) > 300 ? 0 : 20),
            parseFloat(localStorage.getItem("total")) + parseInt(parseFloat(localStorage.getItem('total')) - parseFloat(localStorage.getItem('tax')) > 300 ? 0 : 20),
            this.state.responseData.resultDetails.TransactionIdentfier,
            true,
            this.state.responseData.timestamp
          )
        );
      }
      if (localStorage.getItem("coupon") != "") {
        store.dispatch(setcoupon(JSON.parse(localStorage.getItem("coupon"))));
        localStorage.setItem("coupon", "");
      }
      localStorage.setItem("apply", false);

      window.location.replace("/orders");
    } else {
      return (
        <div>
          <h1>Failed</h1>
          <h3>{this.state.responseData.result.description}</h3>
        </div>
      );
    }
  };
  render() {
    return (
      <div>
        {this.state.loading == false ? this.checkResult() : <h1>Loading</h1>}
      </div>
    );
  }
}

export default ResultPage;

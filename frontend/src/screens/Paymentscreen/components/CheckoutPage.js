import React, { Component } from 'react';
import axios from 'axios';
class CheckoutPage extends Component {
    state = {
        checkoutId: null,
        loading: true
    }
    componentDidMount() {
        axios.post("https://api.makkajperfumes.com/api/checkout", {
            amt: parseFloat(localStorage.getItem('total')) + parseInt(parseFloat(localStorage.getItem('total')) - parseFloat(localStorage.getItem('tax')) > 300 ? 0 : 20),
            crr: localStorage.getItem('curr'),
            address: JSON.parse(localStorage.getItem("address")),
        }).then(res => {
            this.setState({
                checkoutId: res.data.id,
                loading: false
            })
        })
    }
    renderPaymentform = () => {
        const script = document.createElement("script");

        script.src = `https://oppwa.com/v1/paymentWidgets.js?checkoutId=${this.state.checkoutId}`;
        script.async = true;

        document.body.appendChild(script);


        const form = document.createElement("form")
        form.action = "https://makkajperfumes.com/result";
        form.setAttribute("class", "paymentWidgets");
        form.setAttribute("data-brands", "VISA MASTER AMEX MADA")
        document.getElementById('root').insertBefore(form, document.getElementById('foot'));
    }
    render() {
        if (this.state.loading == false) {
            return (
                <div >
                    {this.renderPaymentform()}
                </div>
            );
        } else {
            return (
                <div> Still Loading</div>
            )
        }
    }
}

export default CheckoutPage;

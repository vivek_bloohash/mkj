import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { orderlist } from "../../actions/orderaction";
import { Writereview } from "../writereview/writereview";
import "./Orderconfirmation.css";
import getSymbolFromCurrency from "currency-symbol-map";
import axios from "axios";
import { Translate } from 'react-auto-translate';

export const Orderconfirmation = (props) => {
  const dispatch = useDispatch();
  const orderslist = useSelector((state) => state.orderlist);

  const { orders } = orderslist;
  useEffect(() => {
    if (JSON.parse(localStorage.getItem("login")) == true) {
      dispatch(orderlist(JSON.parse(localStorage.getItem("userInfo")).user));
      axios
        .post("https://api.makkajperfumes.com/api/clearcart", {
          userId: JSON.parse(localStorage.getItem("userInfo")).user,
        })
        .then((res) => {
          
        });




    } else {
      dispatch(orderlist(localStorage.getItem("guestid")));
      axios
        .post("https://api.makkajperfumes.com/api/clearcart", {
          userId: localStorage.getItem("guestid"),
        })
        .then((res) => {
          
        });
    }
  }, [dispatch]);


  useEffect(() => {
    const date = new Date();
    const config = {
      mode: 'no-cors',
      credentials: 'same-origin',
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache',
        'Pragma': 'no-cache'
      },
    };
    if (orders) {
      
      axios
        .post("https://track.smsaexpress.com/SecomRestWebApi/api/addship", {
          "passkey": "ApT@4725",
          "refno": date.getTime(),
          "cPOBox": orders?.shippingAddress?.pin,
          "cName": orders?.shippingAddress?.firstname + " " + orders?.shippingAddress?.lastname,
          "cntry": orders?.shippingAddress?.country,
          "cCity": orders?.shippingAddress?.city,
          "cZip": orders?.shippingAddress?.pin,
          "cMobile": orders?.shippingAddress?.phone,
          "cTel1": orders?.shippingAddress?.phone,
          "cTel2": orders?.shippingAddress?.phone,
          "PCs": orders?.orderItems?.length,
          "cAddr1": orders.shippingAddress.address + " " + orders.shippingAddress.appartment + " " + orders.shippingAddress.city + " " + orders.shippingAddress.state + " " + orders.shippingAddress.country,
          "cAddr2": orders.shippingAddress.address + " " + orders.shippingAddress.appartment + " " + orders.shippingAddress.city + " " + orders.shippingAddress.state + " " + orders.shippingAddress.country,
          "shipType": "DLV",
          "idNo": date.getTime(),
          "cEmail": orders?.shippingAddress?.email,
          "carrValue": orders?.totalPrice,
          "carrCurr": orders?.orderItems[0]?.pricecon,
          "codAmt": orders?.totalPrice,
          "weight": orders?.orderItems[0]?.weight,
          "itemDesc": orders?.orderItems[0]?.description,
          "custVal": orders?.totalPrice,
          "custCurr": orders?.totalPrice,
          "insrAmt": orders?.totalPrice,
          "insrCurr": orders?.orderItems[0]?.pricecon,
          "sName": orders?.shippingAddress?.firstname + " " + orders?.shippingAddress?.lastname,
          "sContact": orders?.shippingAddress?.phone,
          "sAddr1": orders.shippingAddress.address + " " + orders.shippingAddress.appartment + " " + orders.shippingAddress.city + " " + orders.shippingAddress.state + " " + orders.shippingAddress.country,
          "sAddr2": orders.shippingAddress.address + " " + orders.shippingAddress.appartment + " " + orders.shippingAddress.city + " " + orders.shippingAddress.state + " " + orders.shippingAddress.country,
          "sCity": orders?.shippingAddress?.city,
          "sPhone": orders?.shippingAddress?.phone,
          "sCntry": orders?.shippingAddress?.country,
          "sentDate": date.getTime(),
          "prefDelvDate": "3",
          "gpsPoints": "123",
        }, config)
        .then((res) => {
          
        }).catch((err) => {
          console.log(err)
        });
    }
  }, [orders])

  return orders != null ? (
    <div
      style={{
        width: "100%",
        height: "auto",
        borderBottom: "2px solid #c69736",
        paddingBottom: "5rem",
      }}
    >
      <div
        style={{
          width: "100%",
          height: "auto",
          borderTop: "2px solid #fff",
          borderBottom: "2px solid #c69736",
        }}
      >
        <h3
          style={{
            fontWeight: "400",
            color: "#c69736",
            margin: "0.4rem",
            paddingLeft: "1rem",
          }}
        >
          <Translate>ORDER CONFIRMATION</Translate>
        </h3>
      </div>
      <div className="order-top-section1">
        <div className="order-top-logo">
          <img src="./images/logo.png" alt="logo" />
        </div>
        <h1><Translate>YOUR ORDER IS ON THE WAY</Translate></h1>
        <a href="/myorders" className="orderbtn">
          <Translate>MY ORDER</Translate>
        </a>
      </div>

      <div className="order-middle-section">
        <div>
          <h1> <Translate>SUMMARY</Translate></h1>
          <section className="order-id">
            <p> <Translate>ORDER ID</Translate> : </p>
            <p style={{ color: "#fff" }}>{orders._id}</p>
          </section>
          <section className="order-date">
            <p> <Translate>ORDER DATE</Translate> : </p>
            <p style={{ color: "#fff" }}>{orders.createdAt}</p>
          </section>
          <section className="order-total">
            <p> <Translate>ORDER TOTAL</Translate> : </p>
            <p style={{ color: "#fff" }}>
              {getSymbolFromCurrency(orders.orderItems[0].pricecon)}
              {orders.totalPrice}
            </p>
          </section>
        </div>
        <div>
          <h1> <Translate>SHIPPING ADDRESS</Translate></h1>
          <p style={{ paddingLeft: "2rem", color: "#fff" }}>
            {orders?.shippingAddress?.firstname + " " + orders?.shippingAddress?.lastname}
          </p>
          <p style={{ paddingLeft: "2rem", color: "#fff" }}>
            {orders.shippingAddress.address} {orders.shippingAddress.appartment}{" "}
            {orders.shippingAddress.city}
            {orders.shippingAddress.state} {orders.shippingAddress.country}{" "}
          </p>
          <p style={{ paddingLeft: "2rem", color: "#fff" }}>
            {orders?.shippingAddress?.phone}
          </p>

        </div>
      </div>
      <table className="order-table">
        <tr className="order-table-th">
          <th> <Translate>ITEM</Translate></th>
          <th> <Translate>PRICE</Translate></th>
          <th> <Translate>QTY</Translate></th>
          <th> <Translate>STATUS</Translate></th>
        </tr>

        {orders.orderItems.map((item) => {
          return (
            <tr className="order-table-tr">
              <td className="order-table-img">
                <Link to={`/details/${item._id}/${item.title}`}>
                  <img src={item.image} alt="perfume" />
                </Link>
                <h2 style={{ color: "#fff" }}><Translate>{item.title}</Translate></h2>
              </td>
              <td className="order-table-price">
                <p>
                  {getSymbolFromCurrency(item.pricecon)}
                  {item.discountprice[0] * item.count}
                </p>
              </td>
              <td className="order-table-qty">
                <p>{item.count}</p>
              </td>
              <td>
                <p> <Translate>Processing</Translate></p>
              </td>
            </tr>
          );
        })}

        <tr className="below-row">
          <td className="text-right" colSpan="3">
            <span className="row-heading"> <Translate>SUBTOTAL</Translate> : </span>
          </td>
          <td className="text-right" colSpan="4">
            <span>
              {getSymbolFromCurrency(orders.orderItems[0].pricecon)}
              {orders.totalPrice - orders.taxPrice}
            </span>
          </td>
        </tr>
        <tr className="below-row">
          <td className="text-right" colSpan="3">
            <span className="row-heading"> <Translate>SHIPPING CHARGES </Translate>: </span>
          </td>
          <td className="text-right" colSpan="4">
            <span> <Translate>FREE</Translate></span>
          </td>
        </tr>
        <tr className="below-row">
          <td className="text-right" colSpan="3">
            <span className="row-heading"> <Translate>ESTIMATED TAX</Translate> :</span>
          </td>
          <td className="text-right" colSpan="4">
            <span>
              {getSymbolFromCurrency(orders.orderItems[0].pricecon)}
              {orders.taxPrice}
            </span>
          </td>
        </tr>
        <tr className="below-row">
          <td className="text-right" colSpan="3">
            <span className="row-heading"> <Translate>TOTAL AMOUNT</Translate> :</span>
          </td>
          <td className="text-right" colSpan="4">
            <span>
              {getSymbolFromCurrency(orders.orderItems[0].pricecon)}
              {orders.totalPrice}
            </span>
          </td>
        </tr>
      </table>

      {/* <Writereview/> */}
    </div>
  ) : (
    <div></div>
  );
};